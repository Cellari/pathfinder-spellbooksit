# ALPHA

## V0 (Project start)
- Create repository to hold source code and documentation
- Find tools to be used
	- Qt (source code)
	- Qt Quick (UX design)
	- Notepad++ (documentation)
		- Markdown (documentation)
		- PlantUML (UML)
- Create documentation
	- Phases (this documentation)
	- UML
		- Sequence diagrams (user interactions)
			- Create spellbook
			- Selecting a spellbook
		- Class diagrams 
			- Spell views
	- Update project tool instructions to README.md and take the current content to documentations

## V1
- Open Qt
- Create design of tabs
- Create tabs, lists and list items
- Design database structures
- Design event handlers and listeners
- Design test automation. Possible candidate is testoptimal
- Find and use a proper task management. This documentation is lacking in features

## V2
- Automate spell database download
- Find appropriate format for the downloaded db
- Read downloaded database to search tab
- Feature: create spellbooks
- Feature: add spell to spellbook
- Design class diagrams for spells:
	- Small list item
	- Compact list item
	- Large list item

# BETA

# MVP and RELEASE

# FUTURE
- Backup and sync to a cloud service