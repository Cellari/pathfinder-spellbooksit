# Naming conventions

## PlantUML
File names are formatted '[subject] [diagram].puml'. The subject is what every diagram in the file is about eg. database or listmodels. The diagram is either a specified diagram type eg. sequence, class, component or generally diagram. Each puml file can have multiple diagrams. Each file is in lower case.

PlantUML diagram names are formatted '[subject] [topic]', where the subject is the same as in the file name and the topic is the convention that separates diagrams from each other. Each of these diagrams is in lower case. Alternatively the diagram name can be an action, which is useful for eg. sequence diagrams.

## Markdown
Files are simply formatted '[subject].md', where subject is the topic the documentation is about. The contents of a document are not predefined.

# Visual conventions

Grayed out diagram components means they are not part of core functionality. In other words they are not part of MVP (minimum viable product)