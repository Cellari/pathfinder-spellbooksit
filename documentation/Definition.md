# Pathfinder spellbook #

Easy way to find spells and manage them in a spellbook. The application is designed to be open source and uses open source resources. The spells are from a freely available database and are categorized by class, spell level, book, school and domain.

## Details ##
Specific details and design elements to use in the planning of features.

### Target audience ###
Pathfinder players and DMs. Possible variations include DnD 3.0 and 3.5 instead of Pathfinder. What they all want is an easy way to search all spells, have results using different filters, store and manage said spells in a spellbook and manage multiple spellbooks. What they require in addition is the ability to track knowledge, memorisation and usage of a single spell in a stored spellbook.

### Databases ###
Application has two databases: One for spell details and one for the spellbooks. These databases are JSON formatted in a document storages.

Spells and their details are stored in a single database downloaded from the internet. Pathfinder spells are available in a conveniently downloadable database format. The database allows categorisation by class, spell level, book, school and domain.

Each spellbook is stored in one storage. Upon adding a new spell to a spellbook its data is copied, meaning there is duplicate data spread in the spellbooks and in spell list. This way there is a support for legacy spells. In addition each spellbook stores the filter data for spell lists for later use.

### Player settings ###
Possible configurable application settings: Font size, color theme, updating of the spell database

### Revenue ###
Application has a scrollable feed using specific keywords to popular image sites, and amidst those feed items are advertisements using the same keywords to display available products.

## Features ##
Application functionalities and UI elements.

### Tabbed design ###
Users have a single interface to use the application features without the need for back and forth navigation between activities. Swiping left and right is used to navigate between activities.

### Spellbook index tab ###
A.k.a a character spellbook. This list contains the names of stored spellbooks and clicking them opens the spellbook in the contents tab and stored search parameters in the search tab. The index tab also contains a button, which is hilighted if the list is empty, to add a new spellbook. The button opens a dialog, where the user enters a name.

### Spellbook contents tab ###
Each spellbook has their own unique contents and their trackable values. The contents are the spells selected in the search tab for the spellbook. The values for each spell are an adjustable number and a locked number. The locked number is gained by adjusting the number to a desired value and then locking it thus allowing us to memorise the number of spells for a day and easily reset to a previous state.
The spells are ordered by spell level and each spell level divider has a counter derivered from the adjustable numbers combined in the spells of the level in addition to a counter adjustable in the divider, which is in itself independent of how the spell values are set.
Each spell has 3 UI visualisations: First one has the spell name and icons with the counters visible but noneditable; The second one expands from the first one and has a short description of the spell. The last one expands the first one but adds all spell descriptions and allows editing of the counters. In addition a button is added to add a metamagic version of the spell to the spellbook by opening a dialog to name the metamagic feat and adding a number for plus levels, and a button to forget the spell with a confirmation dialogue.
On the top of the list there is a button to reset daily counters, which sets spell adjustable values to the locked values and divider values to 0. A confirmation dialogue is presented to prevent accidental button presses.

### Spell search tab ###
First and foremost spells are searched using filters. There can be multiple filters selectable and are either inclusive or exclusive, which means druidic and domain spells can be both selected and necromancy spells excluded. Filters include classes, schools, books and domains. The results can be orderd by either spell level or spell school. Searching for a spell by a name will first display results from the filtered list, but it will also always display results from the complete spell compendium. The best approach is to have a single line text field for searching and a configuration button next to it.
Each spell item has 3 UI visualisations just like in the spellbook content tab, but without the counters and metamagic button, and a button to add and forget the spell in the spellbook.

### Image and feed tab ###
The feeds are filtered to have content which is related to Pathfinder and role playing games in general. The feeds are from popular source contents like Pinterest and can include DIY items or topics. In between the feed items are adds related to Pathfinder and role playing games in general.

### Syncable spellbooks ###
Backup and sync spellbooks for data integrity. Loss of a device must not result in a lost spellbook. Granting read permissions to other users is useful for dungeon masters to check player spells. Spellbooks are synched between the user devices using cloud services of the platform. Sharing spellbooks are achieved by long pressing the spellbook name and selecting share.