/*
    Copyright © 2017 Ari Suoyrjö

    This file is part of Pathfinder Spellbooksit.

    Pathfinder Spellbooksit is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pathfinder Spellbooksit is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import "qrc:/qml-utils/JSONListModel/"

ListView {
    property string query: ""
    property string source: ""

    id: listView
    model: jsonModel.model
    delegate: ItemDelegate {
        text: name + ": " + short_description
        width: parent.width
        font.pointSize: 15

        Pane {
            width: parent.width
            anchors.bottom: parent.bottom
            Material.elevation: 2
            height: 0
        }
    }

    JSONListModel {
            id: jsonModel
            source: listView.source
            query: listView.query
    }
}
