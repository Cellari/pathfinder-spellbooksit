/*
    Copyright © 2017 Ari Suoyrjö

    This file is part of Pathfinder Spellbooksit.

    Pathfinder Spellbooksit is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pathfinder Spellbooksit is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import "ListViews"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Item {
        anchors.fill: parent

        Keys.onPressed: {
            console.log("Captured key: " + event.key)

            if(event.key === Qt.Key_Escape) {
                Qt.quit()
                event.accepted = true;
            } else if(event.key === Qt.Key_Left ||
                      event.key === Qt.Key_Right) {
                swipeView.Keys.pressed(event)
            }
        }

        SwipeView {
            id: swipeView
            anchors.fill: parent
            anchors.centerIn: parent
            currentIndex: tabBar.currentIndex

            CharacterListView {
                id: characterListView
            }
            SpellListView {
                source: "qrc:/characters.json"
                query: "$.characters[?(@.name=='" + characterListView.selected + "')].spells[*]"
            }
            SpellListView {}
        }

        TabBar {
            id: tabBar
            currentIndex: swipeView.currentIndex
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            Material.elevation: 4

            TabButton {
                Component.onCompleted: forceActiveFocus()
                text: qsTr("Books")
            }
            TabButton {
                text: qsTr("Spellbook")
            }
            TabButton {
                text: qsTr("Spells")
            }
        }
    }
}
