# WORK IN PROGRESS NO FUNCTIONALITIES YET #
# Pathfinder spellbook #

Easy way to find spells and manage them in a spellbook. The application is designed to be open source and uses open source resources. The spells are from a freely available database and are categorized by class, spell level, book, school and domain.

## Project setup and guidance ##

### Workflow ###
The project uses Github Flow. Guide can be found in https://guides.github.com/introduction/flow/ and a short description in http://blog.endpoint.com/2014/05/git-workflows-that-work.html

### Qt ###
Project uses Qt 5.8.0

### Markdown ###
Markdown syntax is used for documentation. To edit a document a compatible editor is recommended, for example Notepad++ with a custom language set for markdown. Proper xml is available in https://github.com/Edditoria/markdown_npp

### PlantUML ###
For UML PlantUML is used, which is a textual UML-conceptualisation format which can be converted to diagrams. Files ending with '.puml' in project are PlantUML-files.

PlantUML requires three things:
	plantuml.jar file to convert .puml files to diagrams from http://plantuml.com/download
	graphviz executable for plantuml.jar to create other diagrams beside sequence. Guide is found in http://plantuml.com/graphviz-dot
	a text editor to edit .puml files. For example Notepad++ with a custom language. That and other tools are found in http://plantuml.com/running

### Visual Studio Code ###
Both Markdown and PlantUML are supported in VSCode, which is a cross platform tool provided by Microsoft. PlantUML is provided through extensions eg. https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml. Both Markdown and PlantUML are provided with a live preview.

## Spell database ##
An Open Game License compliant spell database is available from http://www.pathfindercommunity.net/home/databases/spells

## Depedencies ##
### Submodules ###
After cloning update all submodules with: git submodule update --init --recursive

Current submodules are:
qml-utils (https://github.com/glwu/qml-utils)

# Contact information #
Ari Suoyrjö aka. Cellari (ari.suoyrjo@gmail.com)